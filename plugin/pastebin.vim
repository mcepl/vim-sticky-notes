" ViM Pastebin extension
"
" Original version by Dennis, at
" http://djcraven5.blogspot.com/2006/10/vimpastebin-post-to-pastebin-from.html
"
" Updated in 2008 Olivier Lê Thanh Duong <olethanh@gmail.com>
" to use an XML-RPC interface compatible with http://paste.debian.net/rpc-interface.html
"
" Updated in 2009 for python compatibility by Balazs Dianiska
" <balazs@longlake.co.uk>
"
" Updated in 2010 for pastebin.com compatibility, removed gajim and xmlrpc
" support.
"
" Updated in 2011 to make filetypes work (hack) and full file post work
" and to add automatic user detection by Ben Sinclair
" <ben@moopet.net>
"
" Generalization for paste.fedoraproject.org as well by Matěj Cepl
" 2016, mcepl@cepl.eu
"
" Make sure the Vim was compiled with +python before loading the script...
if !has("python")
        finish
endif

" Map a keystroke for Visual Mode only (default:F2)
:vmap <f2> :PasteCode<cr>

" Send to pastebin
:command! -range             PasteCode :py PBSendPOST(<line1>,<line2>)
" Pastebin the complete file
:command!                    PasteFile :py PBSendPOST()

python << EOF
import getpass
import json
import logging
import os.path
import sys
import urllib
import urllib2
import vim

log = logging.getLogger('vim-paste')
log.setLevel(logging.INFO)

################### BEGIN USER CONFIG ###################
# Set this to your preferred pastebin
service = 'pastefpo'
# Set this to your preferred username, or "auto" to take your shell username
user = 'mcepl'
private = False
email = None
# subdomain (optional) will affect the resulting URL only
subdomain = None
# paste.fedoraproject.org specific
expire = 60 * 60 * 24  # one day
password = None
#################### END USER CONFIG ####################

service_config = {
    'pastebin': {
        'api': 'http://pastebin.com/api_public.php',
        'keys': {
            'user': 'paste_name',
            'code': 'paste_code',
            'format': 'paste_format',
            'subdomain': 'paste_subdomain',
            'email': 'paste_email',
            'private': 'paste_private',
        },
        'filetypes': {
            'bash': ('sh'),
            'c': ('c'),
            'cpp': ('cpp'),
            'css': ('css'),
            'java': ('java'),
            'javascript': ('javascript'),
            'make': ('make'),
            'perl': ('perl'),
            'php': ('php'),
            'python': ('python'),
            'robots': ('robots'),
            'sql': ('sql'),
            'vala': ('vala'),
            'vim': ('vim'),
            'xml': ('xml'),
        }
    },

    'fpaste': {
        'api': 'http://paste.fedoraproject.org/',
        'keys': {
            'user': 'paste_user',
            'code': 'paste_data',
            'format': 'paste_lang',
            'api_submit': True,
            'mode': 'json',
            'expire': 'paste_expire',
            'private': 'paste_private',
            'password': 'paste_password',
        },
        'filetypes': {
            'bash': ('sh'),
            'c': ('c'),
            'cpp': ('cpp'),
            'csharp': ('csharp'),
            'css': ('css'),
            'go': ('go'),
            'html': ('html'),
            'java': ('java'),
            'javascript': ('javascript'),
            'make': ('make'),
            'perl': ('perl'),
            'php': ('php'),
            'python': ('python'),
            'ruby': ('ruby'),
            'robots': ('robots'),
            'sql': ('sql'),
            'vala': ('vala'),
            'vim': ('vim'),
            'xml': ('xml'),
        }
    },

    'pastefpo': {
        'api': 'https://paste.fedoraproject.org/api/paste/submit',
        'keys': {
            'code': 'contents',
            'format': 'language',
            'expire': 'expiry_time',
        },
        'filetypes': {
            'bash': ('sh'),
            'c': ('c'),
            'cpp': ('cpp'),
            'csharp': ('csharp'),
            'css': ('css'),
            'go': ('go'),
            'html': ('html'),
            'java': ('java'),
            'javascript': ('javascript'),
            'make': ('make'),
            'perl': ('perl'),
            'php': ('php'),
            'python': ('python'),
            'ruby': ('ruby'),
            'robots': ('robots'),
            'sql': ('sql'),
            'vala': ('vala'),
            'vim': ('vim'),
            'xml': ('xml'),
        }
    }
}

def get_format_from_filetype(service='pastebin'):
    ft = vim.eval('&ft')
    for name in service_config[service]['filetypes']:
        if ft == service_config[service]['filetypes'][name]:
            return name
    for name in service_config[service]['filetypes']:
        if ft in service_config[service]['filetypes'][name]:
            return name
    return 'text'

def get_user():
    if user == 'auto':
        return getpass.getuser()
    elif user is not None:
        return user
    return 'anonymous'

def shorten_URL(url):
    # curl -v 'https://da.gd/s?url=https://paste.fedoraproject.org/paste/zlNz5~sEE2gboDl4UReHkA'
    dagd_URL = 'https://da.gd/s'
    query_s = urllib.urlencode({'url': url})
    res = urllib2.urlopen('{}?{}'.format(dagd_URL, query_s))
    return res.read().strip()


def PBSendPOST(start=None, end=None):
    if start is None and end is None:
        code = '\n'.join(vim.current.buffer)
    else:
        code = '\n'.join(vim.current.buffer[int(start) - 1:int(end)])

    standard_keys = ('code', 'email', 'expire', 'format', 'password',
                     'private', 'subdomain', 'user',)

    data = {}
    config = service_config[service]
    log.debug('config:\n%s', json.dumps(config))
    data[config['keys']['code']] = code
    data[config['keys']['format']] = get_format_from_filetype(service)
    if 'user' in config['keys']:
        data[config['keys']['user']] = getpass.getuser()
    if subdomain is not None and 'subdomain' in config['keys']:
        data[config['keys']['subdomain']] = subdomain
    if email is not None and 'email' in config['keys']:
        data[config['keys']['email']] = email
    if expire is not None and 'expire' in config['keys']:
        data[config['keys']['expire']] = expire
    if password is not None and 'password' in config['keys']:
        data[config['keys']['password']] = password
    if private and 'private' in config['keys']:
        data[config['keys']['private']] = 1 if private else 0

    # remaining_keys = set(config['keys'].keys()) - set(standard_keys)
    # log.debug('remaining_keys = %s', remaining_keys)
    # for k in remaining_keys:
    #     data[k] = config['keys'][k]

    log.debug('data = %s', data)
    req = urllib2.Request(config['api'], json.dumps(data),
                          headers={'Content-Type': 'application/json'})
    r = urllib2.urlopen(req)
    res = r.read()
    log.debug('res:\n%s', res)

    res_dict = json.loads(res)
    if 'error' in res_dict:
        res = 'Error: %s' % str(res_dict['error'])
    else:
        res = shorten_URL(res_dict['url'])

    print(res)
EOF
